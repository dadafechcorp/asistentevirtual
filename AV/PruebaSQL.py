import mysql.connector

## CONEXION CON LA BBDD (XAMPP) ##
conexion1=mysql.connector.connect(host="localhost", 
                                  user="root", 
                                  passwd="", 
                                  database="asistente")
cursor1=conexion1.cursor()

## INSERTAMOS LA PALABRA Y UNA DESCRIPCION (DESCRIPCION = ACCION A LA QUE LLAMARIA) ##

sql="insert into abrir(texto, descripcion) values (%s,%s)"
datos=("abrete", "Se abre el asistente")
cursor1.execute(sql, datos)
datos=("open", "Se abre el asistente")
cursor1.execute(sql, datos)
conexion1.commit() 

## ELIMINAMOS DATOS ##

cursor1.execute("delete from abrir where texto='open'")

## ACTUALIZAMOS DATOS ##
cursor1.execute("update abrir set descripcion='abrimos el asistente' where texto='abrete'")

## CREARMOS OTA TABLA ##
cursor1.execute("CREATE TABLE cerrar (texto VARCHAR(50) PRIMARY KEY, descripcion VARCHAR(50))")

## ¡¡¡¡¡ IMPORTANTE HACER EL COMMIT !!!!!! ##
conexion1.commit()

## MOSTRAMOS DATOS ##

cursor1.execute("select texto from abrir")
for fila in cursor1:
   print(fila)

## CERRAMOS LA CONEXION ##
conexion1.close()