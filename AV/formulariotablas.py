import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as mb
from tkinter import scrolledtext as st
import formu

class FormularioArticulos:
    def __init__(self):
        self.articulo1=formu.Articulos()
        self.ventana1=tk.Tk()
        self.ventana1.title("Mantenimiento de la tabla abrir")
        self.cuaderno1 = ttk.Notebook(self.ventana1)        
        self.carga_articulos()
        self.listado_completo()
        self.cuaderno1.grid(column=0, row=0, padx=20, pady=20)
        self.ventana1.mainloop()

    def carga_articulos(self):
        self.pagina1 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina1, text="Carga de comandos")
        self.labelframe1=ttk.LabelFrame(self.pagina1, text="Comando")        
        self.labelframe1.grid(column=0, row=0, padx=10, pady=15)
        self.label1=ttk.Label(self.labelframe1, text="texto:")
        self.label1.grid(column=0, row=0, padx=9, pady=14)
        self.descripcioncarga=tk.StringVar()
        self.entrydescripcion=ttk.Entry(self.labelframe1, textvariable=self.descripcioncarga)
        self.entrydescripcion.grid(column=1, row=0, padx=10, pady=10)
        self.label2=ttk.Label(self.labelframe1, text="descripcion:")        
        self.label2.grid(column=0, row=1, padx=10, pady=10)
        self.preciocarga=tk.StringVar()
        self.entryprecio=ttk.Entry(self.labelframe1, textvariable=self.preciocarga)
        self.entryprecio.grid(column=1, row=1, padx=10, pady=10)
        self.boton1=ttk.Button(self.labelframe1, text="Confirmar", command=self.agregar)
        self.boton1.grid(column=1, row=2, padx=10, pady=10)

    def agregar(self):
        datos=(self.descripcioncarga.get(), self.preciocarga.get())
        self.articulo1.alta(datos)
        mb.showinfo("Información", "Los datos fueron cargados")
        self.descripcioncarga.set("")
        self.preciocarga.set("")


    def consultar(self):
        datos=(self.codigo.get(), )
        respuesta=self.articulo1.consulta(datos)
        if len(respuesta)>0:
            self.descripcion.set(respuesta[0][0])
            self.precio.set(respuesta[0][1])
        else:
            self.descripcion.set('')
            self.precio.set('')
            mb.showinfo("Información", "No existe un comando con dicho texto")

    def listado_completo(self):
        self.pagina3 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina3, text="Listado completo")
        self.labelframe3=ttk.LabelFrame(self.pagina3, text="Tabla Abrir")
        self.labelframe3.grid(column=0, row=0, padx=5, pady=10)
        self.boton1=ttk.Button(self.labelframe3, text="Listado completo", command=self.listar)
        self.boton1.grid(column=0, row=0, padx=4, pady=4)
        self.scrolledtext1=st.ScrolledText(self.labelframe3, width=35, height=25)
        self.scrolledtext1.grid(column=0,row=1, padx=10, pady=20)

    def listar(self):
        respuesta=self.articulo1.recuperar_todos()
        self.scrolledtext1.delete("1.0", tk.END)        
        for fila in respuesta:
            self.scrolledtext1.insert(tk.END, "texto:"+fila[0]+"\ndescripcion:"+str(fila[1])+"\n\n")


aplicacion1=FormularioArticulos()